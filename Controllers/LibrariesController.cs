﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AkaLibraryMVC.Models;

namespace AkaLibraryMVC.Controllers
{
    public class LibrariesController : Controller
    {
        private readonly LibraryModel _db = new LibraryModel();

        // GET: Libraries
        public async Task<ActionResult> Index()
        {
            return View(await _db.Libraries.ToListAsync());
        }

        // GET: Libraries/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var library = await _db.Libraries.FindAsync(id);

            if (library == null) return HttpNotFound();

            return View(library);
        }

        public async Task<ActionResult> ReturnBooks()
        {
            var booksSignedOut = await _db.BookSignedOuts
                .Where(bso => bso.WhenReturned == null)
                .ToListAsync();

            foreach (var bookSignedOut in booksSignedOut)
            {
                bookSignedOut.WhenReturned = DateTime.Now;
            }

            _db.SaveChanges();

            return RedirectToAction("Index", "Libraries");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _db.Dispose();
            base.Dispose(disposing);
        }
    }
}