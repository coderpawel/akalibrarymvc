﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AkaLibraryMVC.Models;

namespace AkaLibraryMVC.Controllers
{
    public class LibraryBookController : Controller
    {
        private readonly LibraryModel _db = new LibraryModel();

        // GET: LibraryBook/5
        // where 5- is libraryId

        public async Task<ActionResult> List(int? id)
        {
            ViewBag.LibraryName = _db.Libraries.Find(id).Name;

            var libraryBook = _db.LibraryBook
                .Include(l => l.BookTitle)
                .Include(l => l.Library)
                .Include(l => l.BooksSignedOut)
                .Where(l => l.LibraryId == id);
            var bookList = await libraryBook.ToListAsync();

            bookList = bookList.Where(b => b.AvailableCount > 0).ToList();

            return View(bookList);
        }

        // GET: LibraryBook/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var libraryBook = await _db.LibraryBook.FindAsync(id);

            if (libraryBook == null) return HttpNotFound();

            return View(libraryBook);
        }

        public async Task<ActionResult> SignOutBook(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var libraryBook = await _db.LibraryBook.FindAsync(id);

            if (libraryBook == null) return HttpNotFound();

            var bookSignedOut = new BookSignedOut
            {
                LibraryBookSId = libraryBook.LibraryBookSId,
                LibraryBook = libraryBook
            };

            return View(bookSignedOut);
        }

        [HttpPost]
        public ActionResult PostSignOutBook(BookSignedOut bookSignedOut)
        {
            if (bookSignedOut == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            bookSignedOut.LibraryBook = _db.LibraryBook.Find(bookSignedOut.LibraryBookSId);

            var errors = ValidateBookSignedOut(bookSignedOut);

            if (!errors.Any())
            {
                bookSignedOut.WhenSignedOut = DateTime.Now;

                _db.BookSignedOuts.Add(bookSignedOut);
                _db.SaveChanges();

                return RedirectToAction("List", new {id = bookSignedOut.LibraryBook.LibraryId});
            }

            ViewBag.Errors = errors;

            return View("SignOutBook", bookSignedOut);
        }

        private IEnumerable<string> ValidateBookSignedOut(BookSignedOut bookSignedOut)
        {
            const int bookSignOutMax = 2;
            var errors = new List<string>();

            if (_db.Members.Find(bookSignedOut.MemberId) == null)
            {
                errors.Add("Member Id was invalid");
            }

            var existingBookSignOuts = _db.BookSignedOuts
                .Count(bso => bso.MemberId == bookSignedOut.MemberId && bso.WhenReturned == null);

            if (existingBookSignOuts >= bookSignOutMax)
            {
                errors.Add("Member has reached sign out max limit");
            }

            return errors;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _db.Dispose();
            base.Dispose(disposing);
        }
    }
}