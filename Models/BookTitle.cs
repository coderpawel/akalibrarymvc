using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace AkaLibraryMVC.Models
{
    [Table("BookTitle")]
    public class BookTitle
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BookTitle()
        {
            LibraryBook = new HashSet<LibraryBook>();
        }

        [Key]
        public int BookId { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Isbn { get; set; }

        public DateTime? DateOfPublication { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LibraryBook> LibraryBook { get; set; }
    }
}
