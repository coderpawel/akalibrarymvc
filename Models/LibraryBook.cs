using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace AkaLibraryMVC.Models
{
    [Table("Library_Book")]
    public class LibraryBook
    {
        [Key]
        public int LibraryBookSId { get; set; }

        public int LibraryId { get; set; }

        public int BookId { get; set; }

        public int TotalPurchasedByLibrary { get; set; }

        public virtual BookTitle BookTitle { get; set; }

        public virtual Library Library { get; set; }

        public virtual ICollection<BookSignedOut> BooksSignedOut { get; set; }

        public virtual int AvailableCount
        {
            get
            {
                return TotalPurchasedByLibrary - BooksSignedOut.Count(bso => bso.WhenReturned == null);
            }
        }
    }
}