using System.Data.Entity;

namespace AkaLibraryMVC.Models
{
    public class LibraryModel : DbContext
    {
        public LibraryModel()
            : base("name=LibraryModelConnection")
        {
        }

        public virtual DbSet<BookSignedOut> BookSignedOuts { get; set; }
        public virtual DbSet<BookTitle> BookTitles { get; set; }
        public virtual DbSet<Library> Libraries { get; set; }
        public virtual DbSet<LibraryBook> LibraryBook { get; set; }
        public virtual DbSet<Member> Members { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookTitle>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<BookTitle>()
                .Property(e => e.Isbn)
                .IsUnicode(false);

            modelBuilder.Entity<BookTitle>()
                .HasMany(e => e.LibraryBook)
                .WithRequired(e => e.BookTitle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Library>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Library>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Library>()
                .HasMany(e => e.LibraryBooks)
                .WithRequired(e => e.Library)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Member>()
                .Property(e => e.FullName)
                .IsUnicode(false);

            modelBuilder.Entity<Member>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<Member>()
                .HasMany(e => e.BookSignedOuts)
                .WithRequired(e => e.Member)
                .HasForeignKey(e => e.MemberId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LibraryBook>()
                .HasMany(e => e.BooksSignedOut)
                .WithRequired(e => e.LibraryBook)
                .HasForeignKey(e => e.LibraryBookSId)
                .WillCascadeOnDelete(true);
        }
    }
}
