using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AkaLibraryMVC.Models
{
    [Table("BookSignedOut")]
    public class BookSignedOut
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LibraryBookSId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DisplayName("Member Id")]
        public int MemberId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime WhenSignedOut { get; set; }

        public DateTime? WhenReturned { get; set; }

        public virtual Member Member { get; set; }

        public virtual LibraryBook LibraryBook { get; set; }
    }
}
